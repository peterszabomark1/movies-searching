import * as React from "react";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import InputAdornment from "@mui/material/InputAdornment";
import AutocompleteListItem from "../autocomplete-list-item";
import {
  Box,
  CircularProgress,
  createFilterOptions,
  FilterOptionsState,
} from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { getMoviesBySearchTerm } from "../../helpers/moviesHelpers";
import { debounce } from "throttle-debounce";
import { createStyles, makeStyles } from "@mui/styles";
import { MovieType } from "../../types/movie.type";

export type CoreAutocompleteProps = { setResult: (arg0: MovieType) => void };

const useStyles = makeStyles((theme) =>
  createStyles({
    inputRoot: {
      backgroundColor: "#F9F9FB",
      borderRadius: "2px",
      "&.MuiOutlinedInput-root": {
        padding: "0 9px",
        "& .MuiAutocomplete-input": {
          paddingLeft: "0",
        },
      },
      "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
        borderColor: "inherit",
      },
    },
    inputFocused: { borderColor: "inherit" },
    loading: {
      backgroundColor: "#FFF",
      border: "1px solid black",
    },
    paper: {
      borderColor: "#272933",
      borderRadius: "2px",
      marginTop: "7px",
      boxShadow: "4px 4px 0px 0px #272933",
      backgroundColor: "#FFF",
    },
    option: {
      backgroundColor: "#FFF !important",

      "&.Mui-focused": {
        backgroundColor: "#F9F9FB !important",
        "&:active": {
          backgroundColor: "#E5E5E5 !important",
        },
        "&[aria-selected=true]": {
          backgroundColor: "#FFF",
          "&:active": {
            backgroundColor: "#E5E5E5 !important",
          },
        },
      },
    },
  })
);

export default function CoreAutocomplete(props: CoreAutocompleteProps) {
  const [open, setOpen] = useState<boolean>(false);
  const [options, setOptions] = useState<MovieType[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const debounceRef = useRef<any>();
  const classes = useStyles();

  const onInputChange = (event: unknown, newInputValue: string) => {
    if (newInputValue) {
      if (debounceRef.current) {
        debounceRef.current(newInputValue);
      }
    } else {
      setOpen(false);
      setOptions([]);
    }
  };

  const filterOptions = (options: any, state: FilterOptionsState<unknown>) => {
    const _filterOptions = createFilterOptions();
    const results = _filterOptions(options, state);

    if (results.length > 5) {
      return [
        ...results.splice(0, 5),
        {
          id: "see more",
          title: `See all results for "${state.inputValue}" `,
        },
      ];
    }
    return results;
  };

  useEffect(() => {
    let abortController = new AbortController();
    let signal = abortController.signal;

    const loadData = async (input: string) => {
      // Signal is already aborted, so we don't need to perform search
      if (abortController.signal.aborted) {
        return;
      }

      try {
        setOptions([]);
        setIsLoading(true);

        let selectedMovies: MovieType[] = await getMoviesBySearchTerm(
          input,
          signal
        );

        setOptions(selectedMovies);
      } catch (error) {
        // Sentry would be useful to catch this error
      } finally {
        setIsLoading(false);
      }
    };

    debounceRef.current = debounce(350, loadData);

    return () => {
      abortController.abort();
      debounceRef.current = undefined;
    };
  }, [debounceRef]);

  useEffect(() => {
    if (!open) {
      setOptions([]);
      setIsLoading(false);
    }
  }, [open]);

  return (
    <Autocomplete
      classes={classes}
      id="movie-select"
      sx={{ width: `100%` }}
      options={options}
      freeSolo
      disableClearable
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      onChange={(e, option: any) => {
        if (option.id) {
          props.setResult(option);
        }
      }}
      onInputChange={onInputChange}
      loading={isLoading}
      loadingText={
        <Box
          sx={{
            display: `flex`,
            alignItems: "center",
            justifyContent: `center`,
            height: `538px`,
          }}
        >
          <CircularProgress sx={{ color: "#000" }} size={40} />
        </Box>
      }
      isOptionEqualToValue={(option: any, value: any) =>
        option.title === value.title
      }
      getOptionLabel={(option: any) => (option.title ? option.title : "")}
      filterOptions={filterOptions}
      renderOption={(props, option: any) => (
        <AutocompleteListItem option={option} {...props} />
      )}
      ListboxProps={{
        style: {
          maxHeight: "550px",
          border: "1px solid black",
          borderRadius: "2px",
        },
      }}
      renderInput={(params) => (
        <>
          <TextField
            onChange={(e) => {
              setOptions([]);
              setIsLoading(true);
            }}
            {...params}
            placeholder="Search..."
            InputProps={{
              ...params.InputProps,
              startAdornment: (
                <InputAdornment position="start">
                  <SearchIcon sx={{ ml: 1, color: "#000" }} />
                </InputAdornment>
              ),
            }}
          />
        </>
      )}
    />
  );
}
