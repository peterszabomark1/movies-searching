import { Box, Typography } from "@mui/material";
import Chip from "@mui/material/Chip";
import CircleIcon from "@mui/icons-material/Circle";
import { MovieType } from "../../types/movie.type";

export type ResultContentProps = { result?: MovieType };

export default function ResultContent(props: ResultContentProps) {
  const showResult: boolean | undefined =
    props.result && props.result.id !== "see more";

  return (
    <>
      {showResult && (
        <Box
          aria-label="result-content"
          sx={{
            display: "grid",
            width: "100%",
            marginTop: "3rem",
            gridTemplateColumns: "1fr 2fr",
            columnGap: "2rem",
          }}
        >
          <Box>
            <img
              loading="lazy"
              width="100%"
              src={props.result?.img}
              srcSet={`${props.result?.img} 2x`}
              alt=""
            />
          </Box>
          <Box>
            <Typography
              sx={{
                fontFamily: `"Rubik", sans-serif`,
                fontWeight: (theme) => theme.typography.fontWeightBold,
              }}
              variant="h3"
            >
              {props.result?.title}
            </Typography>
            <Typography
              sx={{ display: "flex", mt: 2, mb: 4 }}
              variant="subtitle1"
            >
              {props.result?.date_of_release}
              <CircleIcon
                sx={{ fontSize: "5px", alignSelf: "center", ml: 2, mr: 2 }}
              />
              {props.result?.ageLimit}
              <CircleIcon
                sx={{ fontSize: "5px", alignSelf: "center", ml: 2, mr: 2 }}
              />
              {props.result?.time}
            </Typography>
            <Typography sx={{ mt: 2, mb: 2 }} variant="body1">
              {props.result?.description}
            </Typography>
            <Box sx={{ mt: 4 }}>
              {props.result?.tags?.map((tag, index) => (
                <Chip
                  sx={{ mr: 2, backgroundColor: "#FFAA00" }}
                  key={index}
                  label={tag}
                />
              ))}
            </Box>
          </Box>
        </Box>
      )}
    </>
  );
}
