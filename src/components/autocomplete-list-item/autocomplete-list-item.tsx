import { Box, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { MovieType } from "../../types/movie.type";

export type AutocompleteListItemProps = { option: MovieType };

export default function AutocompleteListItem(props: AutocompleteListItemProps) {
  const [showSeeMore, setShowSeeMore] = useState<boolean>(false);

  useEffect(() => {
    if (props.option.id === "see more") {
      setShowSeeMore(true);
    }
  }, [props]);

  return (
    <>
      {showSeeMore ? (
        <Box
          component="li"
          sx={{
            height: 50,
          }}
          {...props}
        >
          <Typography
            sx={{
              mb: 0.5,
              fontWeight: (theme) => theme.typography.fontWeightBold,
            }}
          >
            {props.option.title}
          </Typography>
        </Box>
      ) : (
        <Box
          component="li"
          aria-label={props.option.title}
          sx={{
            height: 100,
            borderBottom: "2px solid #F2F2F2",
          }}
          {...props}
        >
          <Box sx={{ display: `flex`, alignItems: `center` }}>
            <Box sx={{ mr: 2.5 }}>
              <img
                loading="lazy"
                width="50"
                height="100%"
                src={props.option.img}
                srcSet={`${props.option.img} 2x`}
                alt=""
              />
            </Box>
            <Box>
              <Typography
                sx={{
                  mb: 0.5,
                  fontWeight: (theme) => theme.typography.fontWeightBold,
                }}
              >
                {props.option.title}
              </Typography>
              <Typography
                sx={{
                  mb: 0.5,
                  fontWeight: (theme) => theme.typography.fontWeightBold,
                }}
                variant="body2"
              >
                {props.option.date_of_release}
              </Typography>

              <Typography variant="subtitle2">
                {props.option.actors?.map((actor, index) => (
                  <Box
                    key={index}
                    sx={{ mr: 1, color: "#5C6070" }}
                    component="span"
                  >
                    {actor}
                    {props.option.actors.length - 1 === index ? null : ","}
                  </Box>
                ))}
              </Typography>
            </Box>
          </Box>
        </Box>
      )}
    </>
  );
}
