import { rest } from "msw";
import movies from "../data/movies.json";

// This handler give the movies by searh term with 1 second delay 
export const handlers = [
  rest.get("/movies", (req, res, ctx) => {
    const searchTerm = req.url.searchParams.get("search");
    const filteredResponse = movies.filter(
      (movie) =>
        movie.title.toLowerCase().search(searchTerm.toLocaleLowerCase()) >= 0
    );

    return res(ctx.delay(1000), ctx.status(200), ctx.json(filteredResponse));
  }),
];
