import { createTheme } from "@mui/material";
import { ThemeProvider } from "@mui/styles";
import "./App.css";
import SearchPage from "./pages/search";

const theme = createTheme({
  typography: {
    fontFamily: `"Roboto", sans-serif`,
  },
});
function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <SearchPage />
      </div>
    </ThemeProvider>
  );
}

export default App;
