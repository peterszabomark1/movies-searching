import { rest } from "msw";
import movies from "../../data/movies.json";

const BASE_URL = "http://localhost:3000";

export const fetchMoviesBySearchTermHandler = (searchTerm: string) =>
  rest.get(`${BASE_URL}/movies`, (_req, res, ctx) => {
    const filteredMovies = movies.filter(
      (movie) =>
        movie.title.toLowerCase().search(searchTerm.toLocaleLowerCase()) >= 0
    );

    return res(ctx.json(filteredMovies));
  });
