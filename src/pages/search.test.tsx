import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import SearchPage from "./search";
import { setupServer } from "msw/node";
import { fetchMoviesBySearchTermHandler } from "../tests/handlers/movies-handler";

const server = setupServer();

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test(`should not render any options, when type incorrect movie title`, async () => {
  render(<SearchPage />);

  const searchField = screen.getByRole(/textbox/i);

  expect(searchField).toBeInTheDocument();

  server.use(fetchMoviesBySearchTermHandler("jurassic"));

  userEvent.type(searchField, "jurassic");

  expect(
    await screen.findByRole(`option`, { name: /jurassic park III/i })
  ).toBeInTheDocument();

  //search a word when we expect that the response has zero option
  userEvent.type(searchField, "juru");

  expect(
    screen.queryByRole(`option`, { name: /jurassic park III/i })
  ).not.toBeInTheDocument();

  expect(screen.queryAllByRole(`option`)).toHaveLength(0);
});

test(`should render an option with name 'See all result for "Jurassic"', when found more then five movies`, async () => {
  render(<SearchPage />);

  const searchField = screen.getByRole(/textbox/i);

  expect(searchField).toBeInTheDocument();

  server.use(fetchMoviesBySearchTermHandler("jurassic"));

  userEvent.type(searchField, "jurassic");

  expect(
    await screen.findByText(/see all results for "jurassic"/i)
  ).toBeInTheDocument();
});

test("should render a movie deatils, when we select a movie from list", async () => {
  render(<SearchPage />);

  const searchField = screen.getByRole(/textbox/i);

  //result content does not appear on the screen
  expect(screen.queryByLabelText("result-content")).not.toBeInTheDocument();

  expect(searchField).toBeInTheDocument();

  server.use(fetchMoviesBySearchTermHandler("jurassic"));

  userEvent.type(searchField, "jurassic");

  expect(
    await screen.findByRole(`option`, { name: /jurassic park III/i })
  ).toBeInTheDocument();

  userEvent.click(
    await screen.findByRole(`option`, { name: /jurassic park III/i })
  );

  //result content is displayed on the screen
  expect(await screen.findByLabelText("result-content")).toBeInTheDocument();

  expect(
    await screen.findByRole("heading", {
      name: /jurassic park III/i,
    })
  ).toBeInTheDocument();
});
