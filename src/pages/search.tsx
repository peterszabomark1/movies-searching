import { Box } from "@mui/material";
import { useState } from "react";
import CoreAutocomplete from "../components/autocomplete";
import ResultContent from "../components/result-content";
import { MovieType } from "../types/movie.type";

export default function SearchPage() {
  const [result, setResult] = useState<MovieType>();

  return (
    <Box
      sx={{
        width: "50vw",
        "@media(max-width: 768px)": {
          width: "80vw"
        },
      }}
    >
      <CoreAutocomplete setResult={setResult} />
      <ResultContent result={result} />
    </Box>
  );
}
