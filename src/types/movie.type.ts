export type MovieType = {
  id: number | string;
  title: string;
  img: string;
  date_of_release: string;
  actors: Array<string>;
  description: string;
  time: string;
  tags: Array<string>;
  ageLimit: number;
};
