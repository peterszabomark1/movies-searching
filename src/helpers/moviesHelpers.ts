const BASE_URL = "http://localhost:3000";

export async function getMoviesBySearchTerm(searchTerm: string, signal: any) {
  try {
    const response = await fetch(`${BASE_URL}/movies?search=${searchTerm}`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      signal,
    });

    return response.json();
  } catch (error) {
    //Sentry would be useful to catch this error
    console.log(error);
  }
}
