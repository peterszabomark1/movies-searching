# Movies searching


## Description
You have the opportunity to learn more about the movies mentioned below. Use the search box to find a movie and click an option.

**Movies:**
* Jurassic Park
* Jurassic World
* Jurassic Park III
* Jurassic Park: The Lost World
* Jurassic World: Fallen Kingdom
* Jurassic World Dominion
* Inception
* The Dark Knight
* The Dark Knight Rises
* Joker
* The Wolf of Wall Street
* Interstellar


## Run project

Proposed development environment:

* node: 16.13.2
* npm: 8.1.2
* IDE: Visual Studio Code

In the project directory, you can run:

### 1. Step: `npm install`

This command installs a package, and any packages that it depends on. 

### 2. Step: `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Run tests

In the project directory, you can run:
### `npm test`

Launches the test runner in the interactive watch mode.